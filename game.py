from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText
from panda3d.core import TextNode, TransparencyAttrib
from panda3d.core import LPoint3, LVector3
from direct.task.Task import Task
from panda3d.core import AmbientLight, DirectionalLight, LightAttrib
from enemiesspawner import EnemiesSpawner
import commonmethods
import sys

SHIP_SPEED = 20

	


	
class Game(ShowBase):
 
	def __init__(self):
		ShowBase.__init__(self)
		
		self.score = 0
		
		self.title = OnscreenText(text="3D Game",parent=base.a2dBottomRight, scale=.07,align=TextNode.ARight, pos=(-0.1, 0.1),fg=(1, 1, 1, 1), shadow=(0, 0, 0, 0.5))
		self.scoreText = OnscreenText(text="Score: " + str(self.score),parent=base.a2dTopLeft, scale=.07,align=TextNode.ALeft, pos=(0.1, -0.1),fg=(1, 1, 1, 1), shadow=(0, 0, 0, 0.5))
		
		
		self.setupLights()
		
		self.disableMouse()

		self.setBackgroundColor((0, 0, 0, 1))
		
		self.bg = commonmethods.loadObject("plane","stars.jpg", scale=146, depth=200,transparency=False)
		self.bg.setScale(300)
		
		self.ship = commonmethods.loadObject("knight",None)
		self.ship.setPos(0,	55,-20)
		self.ship.setScale(4)
		self.ship.setHpr(0,90,0);
		self.bullets = list()
		
		self.setVelocity(self.ship, LVector3.zero())
		
		self.keys = {"turnLeft": 0, "turnRight": 0, "fire": 0}
				
		self.accept("escape", sys.exit)
		self.accept("arrow_left",     self.setKey, ["turnLeft", 1])
		self.accept("arrow_left-up",  self.setKey, ["turnLeft", 0])
		self.accept("arrow_right",    self.setKey, ["turnRight", 1])
		self.accept("arrow_right-up", self.setKey, ["turnRight", 0])
		self.accept("space",          self.setKey, ["fire", 1])
		
		self.gameTask = taskMgr.add(self.gameLoop, "gameLoop")
		camera.setPos(0,10,-40)
		camera.setHpr(0,30,0)
		
		self.enemiesList = list()
		self.enemiesSpawner = EnemiesSpawner(self.enemiesList)

	def setupLights(self):  # This function sets up some default lighting
		ambientLight = AmbientLight("ambientLight")
		ambientLight.setColor((.8, .8, .8, 1))
		directionalLight = DirectionalLight("directionalLight")
		directionalLight.setDirection(LVector3(0, 45, -45))
		directionalLight.setColor((0.2, 0.2, 0.2, 1))
		render.setLight(render.attachNewNode(directionalLight))
		render.setLight(render.attachNewNode(ambientLight))
		
	def gameLoop(self, task):
		dt = globalClock.getDt()
	
		if self.keys["fire"]:
			self.fire()
		self.keys["fire"] = 0
		
		self.updateShip(dt)
		self.updateEnemies(dt)
		self.updateBullets(dt)
		self.enemiesSpawner.Update(dt);
		self.checkCollisions()
		
		self.scoreText.setText(text="Score: " + str(self.score))

		return Task.cont
		
	def updateShip(self, dt):
		if self.keys["turnRight"]:
			newpos = self.ship.getPos() + LVector3(1 * dt * SHIP_SPEED,0,0)
			if (newpos.x > 15):
				newpos.x = 15
			self.ship.setPos( newpos)
		elif self.keys["turnLeft"]:
			newpos = self.ship.getPos() +  LVector3(-1 * dt * SHIP_SPEED,0,0)
			if (newpos.x < -15):
				newpos.x = -15
			self.ship.setPos( newpos)
			
	def updateEnemies(self,dt):
		enemiesToRemove = list()
	
		for enemy in self.enemiesList:
			currentPos = enemy.getPos()
			currentPos.z -= enemy.getPythonTag("speed") * dt
			enemy.setPos(currentPos)
			if(currentPos.z < -28):
				enemiesToRemove.append(enemy)
		
		for enemyToRemove in enemiesToRemove:
			self.enemiesList.remove(enemyToRemove)
			enemyToRemove.removeNode()
			del enemyToRemove
			self.score -=20
						
	def setKey(self, key, val):
		self.keys[key] = val

		
	def setVelocity(self, obj, val):
		obj.setPythonTag("velocity", val)

	def getVelocity(self, obj):
		return obj.getPythonTag("velocity")

	def fire(self):
		pos = self.ship.getPos()
		bullet = commonmethods.loadObject("pawn",None, scale=2)  
		bullet.setPos(pos)
		bullet.setPythonTag("speed", 20)
		self.bullets.append(bullet)
			
	def updateBullets(self,dt):
		bulletsToRemove = list()
	
		for bullet in self.bullets:
			currentPos = bullet.getPos()
			currentPos.z += bullet.getPythonTag("speed") * dt
			bullet.setPos(currentPos)
			if(currentPos.z > 5 ):
				bulletsToRemove.append(bullet)
		
		for bulletToRemove in bulletsToRemove:
			self.bullets.remove(bulletToRemove)
			bulletToRemove.removeNode()
			del bulletToRemove
		
			
	def checkCollisions(self):
		bulletsToRemove = list()
		enemiesToRemove = list()
	
		for bullet in self.bullets:
			for enemy in self.enemiesList:
				bulletPos = bullet.getPos()
				enemyPos = enemy.getPos()
				if (abs(bulletPos.x - enemyPos.x)<0.8 and abs(bulletPos.z - enemyPos.z)<0.8):
					bulletsToRemove.append(bullet)
					enemiesToRemove.append(enemy)
					self.score +=10

		
					
		for bulletToRemove in bulletsToRemove:
			self.bullets.remove(bulletToRemove)
			bulletToRemove.removeNode()
			del bulletToRemove
			
		for enemyToRemove in enemiesToRemove:
			self.enemiesList.remove(enemyToRemove)
			enemyToRemove.removeNode()
			del enemyToRemove
			
app = Game()
app.run()