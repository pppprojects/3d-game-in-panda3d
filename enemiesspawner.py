
import commonmethods
from random import randint

class EnemiesSpawner():
	def __init__(self,enemiesList):
		self.enemiesList = enemiesList
		self.timeFromLastSpawn = 0 
		self.timeToSpawnNext = 1.5
		
	def Update(self,dt):
		self.timeFromLastSpawn += dt;
		
		if self.timeFromLastSpawn > self.timeToSpawnNext:
			self.SpawnNext()
			self.timeFromLastSpawn = 0
			self.timeFromLastSpawn = 0
			self.timeToSpawnNext = randint(5,18)/10.0
		
	def SpawnNext(self):
		xPosition= randint(-15,15)
		speed= randint(1,5)
		
		newEnemy = commonmethods.loadObject("king",None)
		newEnemy.setPos(float(xPosition),55,0)
		newEnemy.setScale(4)
		newEnemy.setHpr(0,90,0)
		newEnemy.setPythonTag("speed", speed)
		self.enemiesList.append(newEnemy)